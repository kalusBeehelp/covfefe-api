import time
import json
import subprocess
from w1thermsensor import W1ThermSensor

class TemperatureService():

    def __init__(self):
        subprocess.call(['modprobe', 'w1-gpio'])
        subprocess.call(['modprobe', 'w1-therm'])
        # subprocess.call(['sudo', 'dtoverlay', 'w1-gpio gpiopin=17'])
        self.sensor = W1ThermSensor()

    def get_current_temp(self):
        temperature = self.sensor.get_temperature()
        # text = "The temperature is %s celsius" % temperature
        return temperature

    def get_config_temp(self, type):
        with open("database.json", "r") as jsonFile:
            data = json.load(jsonFile)
            return data['heatConfig'][type]


    def update_temp(self, temp, type):
        with open("database.json", "r") as jsonFile:
            data = json.load(jsonFile)
            data["heatConfig"][type] = temp

        with open("database.json", "w") as jsonFile:
            json.dump(data, jsonFile)
        
        return data
