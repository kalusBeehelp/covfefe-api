from flask import Flask, jsonify, request
from flask_cors import CORS, cross_origin
import RPi.GPIO as  GPIO
from relay import RelayService
from temperature import TemperatureService
from subprocess import call
import time
import sys


GPIO.setmode(GPIO.BCM) 
app = Flask(__name__)
cors = CORS(app)
app.config['CORS_HEADERS'] = 'Content-Type'

temperatureService = TemperatureService()
relayService = RelayService(temperatureService)

@app.route('/pin/<id>')
@cross_origin()
def pin(id):
    print(id)
    relayService.switch_state(id)
    res = relayService.get_all_states()
    return jsonify(res)

@app.route('/status')
@cross_origin()
def status():
    res = relayService.get_all_states()
    return jsonify(res)

@app.route('/status/<id>')
@cross_origin()
def status_by_id(id):
    channel = relayService.get_channel(id)
    state = relayService.get_state(channel)
    payload = {
        'state': state,
        'channel': channel,
        'pin': int(id)
    }
    return jsonify(payload)

@app.route('/poweroff')
@cross_origin()
def poweroff():
    relayService.stop()
    res = relayService.get_all_states()
    call(["sudo","/usr/sbin/poweroff"])
    return jsonify(res)

@app.route('/idle')
@cross_origin()
def idle():
    relayService.stop()
    res = relayService.get_all_states()
    return jsonify(res)


@app.route('/exec/<elem>/<state>/<type>')
@cross_origin()
def exec_elem(elem, state, type):
    print(elem)
    if elem == 'heater':
        print('on allume le chauffage router', file=sys.stderr)
        relayService.exec_heater(state, type)
        # time.sleep(int(time_on))
        # print('on éteint le chauffage router', file=sys.stderr)
        # relayService.exec_heater('off')
    elif elem == 'pump':
        relayService.exec_pump('on', type)
        time.sleep(int(time_on))
        relayService.exec_pump('off')
    
    return jsonify(relayService.get_all_states())

@app.route('/make-coffee/<type>')
@cross_origin()
def make_coffee(type):
    if type == 'long':
        relayService.exec_heater('on')
        time.sleep(30)
        relayService.exec_pump('on')
        time.sleep(20)
        relayService.exec_pump('off')
        relayService.exec_heater('off')
        # relayService.idle_timeout()
    return jsonify(relayService.get_all_states())

@app.route('/temperature/<type>', methods = ['GET', 'PATCH'])
@cross_origin()
def get_temperature(type):
    if request.method == 'GET':
        if type == 'current':
            # On va chercher la temperature actuelle
            return jsonify(temperatureService.get_current_temp())
        elif type == 'config':
            # On va chercher la temperature set dans la config
            return jsonify(temperatureService.get_temp(type))
    elif request.method == 'PATCH':
        return jsonify(temperatureService.update_temp(int(type)))
