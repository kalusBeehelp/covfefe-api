import RPi.GPIO as  GPIO
import time
import sys

class RelayService():

    def __init__(self, temperature_service):
        self.is_idle = False
        self.state = [False, False, False, False]
        self.channels = [4, 22, 6, 26]
        self.elements = ["Chauffage", "Pompe", "Thermostat / Vapeur", "TDB"]
        self.heater_status = False
        self.temperature_service = temperature_service
        self.temperature = 0
        self.start()



    def set_heater(self, type):
        while self.state[0]:
            current_temp = self.temperature_service.get_current_temp()
            config_temp = self.temperature_service.get_config_temp(type)
            if current_temp >= config_temp:
                GPIO.output(26, False)
                GPIO.output(6, False)
            else:
                GPIO.output(26, True)
                if current_temp >= 70:
                    GPIO.output(6, True)




        time.sleep(1)

    
    def exec_heater(self, state, type):
        if state == 'on' and not self.state[0]:
            GPIO.output(26, True)
            self.state[0] = True
            self.set_heater(type)
        elif state == 'off':
            GPIO.output(26, False)
            self.state[0] = False
            return 0
        else:
            return 'error'

            
        


    def exec_pump(self, slug):
        if slug == 'on':
            GPIO.output(22, True)
            self.state[1] = True
            return 1
        elif slug == 'off':
            GPIO.output(22, False)
            self.state[1] = False
            return 0

    def start(self):
        print(GPIO.RPI_INFO, file=sys.stderr)
        for channel in self.channels:
            GPIO.setup(channel, GPIO.OUT)
            GPIO.output(channel, False)

    def stop(self):
        for channel in self.channels:
            GPIO.output(channel, False)
        self.state = [False, False, False, False]

    def switch_state(self, id):
        channel = self.get_channel(id)
        current_state = self.get_state(channel)
        GPIO.output(channel, not current_state)
        self.state[int(id)-1] = not current_state

    def get_all_states(self):
        res = []
        for i, channel in enumerate(self.channels, start=1):
            state = self.get_state(channel)
            payload = {
                'state':state,
                'channel': channel,
                'pin': i,
                'elem': self.elements[i-1]
            }
            res.append(payload)
        return res

    def get_state(self, channel):
        # To test the value of a pin use the .input method
        channel_is_on = GPIO.input(channel) ## Returns 0 if OFF or 1 if ON
        if channel_is_on:
            return True
        else:
            return False

    def get_channel(self, id):
        if id == '1':
            return 26
        elif id == '2':
            return 22
        elif id == '3':
            return 6
        elif id == '4':
            return 4

    def idle_timeout(self):
        self.is_idle = True
        time.sleep(90)
        self.stop()