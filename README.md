# Covfefe Api

Python api for coffee maker controle via Raspberry gpio

## Stack

- Python 3.7.3
- Flask

## Todo

- Faire un café : Allumer le chauffage et attendre 30 secondes, then on allume la pompe pdt 30 secondes
- Mettre un timeout de 9 minutes a la fin du café
- Mode nettoyage
- Chauffage en fonction de la temperature

## Définition des pins

- 4 : Chauffage 
- 2 : Pompe
- 3 : Thermostat / Vapeur
